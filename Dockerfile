FROM centos:7
MAINTAINER Sucipto <chip@pringstudio.com>

# Set Env
ENV ORACLE_HOME=/usr/lib/oracle/12.1/client64

ENV LD_LIBRARY_PATH="$ORACLE_HOME/lib"

# Include Oracle Client
COPY oracle/*.rpm oracle-client.rpm

# Add Repo
RUN yum install sudo -y && \
    sudo rpm -Uvh http://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm && \
    sudo rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm && \
    sudo yum update -y && \
    sudo yum install httpd php71 php71-devel libaio gcc php71-php-oci8 php71-php-mbstring php71-php-mcrypt php71-php-gd -y && \
    sudo yum install ./oracle-client.rpm -y

# Remove RPM
RUN rm ./oracle-client.rpm

# Link PHP Binary
RUN ln -s /usr/bin/php71 /usr/bin/php

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/bin/composer

# HTTPD Config
RUN sed -i 's#/var/www/html#/var/www/laravel/public#g' /etc/httpd/conf/httpd.conf

# HTTPD Foreground Script
COPY httpd-foreground /usr/local/bin/

# Expose Port
EXPOSE 80

# Run HTTPD on docker run
CMD ["httpd-foreground"]